# encoding: UTF-8

def get_messages_from(timestamp)
  Skyper::Chat.recent_chats.map(&:recent_chat_messages).flatten.select{|c| c.timestamp > timestamp}
end


def get_chat_from_message(message)
  Skyper::Chat.recent_chats.find(:id => message.id).first
end

def get_chat_by_id(id)
  Skyper::Chat.recent_chats.select{|x| x.id == "#{id}"}.first
end


def proc_message(message)
  TRIGGERS.each do |trigger, options|
    if message.body.include?(trigger)
      method = options[:method]
      @chat = get_chat_from_message(message)
      send(method, message, @chat)
    end
  end  
end


def get_fullname(member)
  Skyper::Skype.send_command("GET USER #{member} FULLNAME").split("FULLNAME")[1].strip.gsub!(" ","%20")
end


def get_ngnix_info(line)
  time = line.split("\"")[0].split(" - - ")[1].strip[1..-2].split(" ")[0]
  time = DateTime.strptime(time, "%d/%b/%Y:%H:%M:%S").to_time
  ip = line.split(" - - ")[0]
  route = line.split("\"")[1]
  diff = Time.at(Time.now - time).gmtime.strftime('%Hh %Mm %Ss')
  return time, diff, route
end











