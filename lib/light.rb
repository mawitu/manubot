# encoding: UTF-8

TRIGGERS = {} if !defined?(TRIGGERS)
TRIGGERS.merge!({
  "push button" =>  {
    :method      =>  "push_button",
    :status      =>  'in use',    
    :description =>  "Deploy latest changes into osp.test.avallain.com",
    :shortcut    =>  'pbtn'
  },
  "pbtn" =>  {
    :method     =>  "push_button",
    :status     =>  'shortcut'
  },  
  "gimme shit"  =>  {
    :method      =>  "gimme_shit",
    :status      =>  'in use',
    :description =>  "Deploy latest changes into osp.test.avallain.com",
    :shortcut    =>  'gdb'
  },
  "gdb" => {
    :method     =>  "gimme_shit",
    :status     =>  'shortcut'
  },
  "gimme logshit" =>  {
    :method     =>  "gimme_logshit",
    :status     =>  'in use',
    :description=>  "Share into the chat a link to download osp.test.avallain.com log",
    :shortcut   =>  'glog'
  },    
  "glog" =>  {
    :method     =>  "gimme_logshit",
    :status     =>  'shortcut'
  },
  "working on test" => {
    :method      =>  'working_on_test',
    :status      =>  'deprecated',
    :description =>  '[DEPRECATED] Show latest 3 calls from osp.test.avallain.com'
  },
  "wot_old" =>  {
    :method      =>  'working_on_test',
    :status      =>  'deprecated',
    :description =>  '[DEPRECATED] Show latest 3 calls from osp.test.avallain.com'
  },
  "wot?" =>  {
    :method      =>  'working_on_test_ngnix',
    :status      =>  'in use',
    :description =>  'Show latest 3 calls from osp.test.avallain.com'
  },
  "trollci" =>  {
    :method      =>  'ci',
    :status      =>  'pending',
    :description =>  'Show jasmine test into the chat'
  },
  "testing, madafaka" => {
    :method      => "test_status",
    :status      => 'in use',
    :description => 'Show test status into the chat',
    :shortcut    => 'sts'
  },
  "sts" => {
    :method      =>  "test_status",
    :status      =>  'shortcut'
  },
  "help me, mb!" =>  {
    :method      =>  "show_help",
    :status      =>  'in use',
    :description =>  'Show help, with commands and shortcuts from MB',
    :shortcut    =>  'helpmb'
  },
  "helpmb" =>  {
    :method      =>  "show_help",
    :status      =>  'shortcut'
  },
  "jokemb" =>  {
    :method      =>  "show_dark_help",
    :status      =>  'in use',
    :description =>  'Show dark commands of MB'
  }
})

def show_commands(chat,status_condition='in use')
  chat.chat_message "MB to the rescue! Remember, to use commands just write them downcased: "
  TRIGGERS.each do |trigger, options|
    if options[:status] == status_condition
      message = '* ' + trigger.upcase
      message += '" [' + options[:shortcut].upcase + ']' if options[:shortcut]
      message += ': ' + options[:description]
      chat.chat_message message
    end
  end   
end

def show_help(message, chat)
  show_commands chat
end

def show_dark_help(message, chat)
  show_commands(chat, 'dark')
end

def push_button(message, chat)
  server = @config["server"]
  slave = @config["server"]

  chat.chat_message("MANUBOT SAYS: Pushing button!")
  pull_repo = `cd repos/ouposp && git pull && cd ..`

  deploy = `ey deploy --account='Avallain' --app='oup_osp' --environment='meta3_testing' --ref='refs/heads/develop' --migrate='rake db:migrate'`

  if !deploy.include?("Failed deployment recorded on Engine Yard Cloud")
    message =  "MANUBOT SAYS: Deploy done. Restarting!"
  else
    message = "MANUBOT SAYS: Deploy failed! Shame on you! (finger)"
  end

  chat.chat_message(message)

  restart = `ssh deploy@#{server} 'cd /data/oup_osp/current/ && RAILS_ENV=staging bundle exec rake i18n:export && rm -rf public/assets/ && touch tmp/restart.txt'`
  delayed = `ssh deploy@#{server} 'sudo monit stop delayed_job1_0 && sudo monit start delayed_job1_0'`
  # restart_slave = `ssh deploy@#{slave} 'cd /data/oup_osp/current/ && RAILS_ENV=staging bundle exec rake i18n:export && rm -rf public/assets/ && touch tmp/restart.txt'`

  waiting_for_restart = `curl --silent http://#{server}/app/`
  chat.chat_message("MANUBOT SAYS: Server restarted")

  terminal_results = `osascript -e '#{'tell application "Terminal" to return name of window 2'}'`
  chat.chat_message("MANUBOT SAYS: Test status #{terminal_results}")

end

def gimme_shit(message, chat)
  server = @config["server"]
  slave = @config["slave"]
  balance = @config["balance"]

  host = @config["host"]
  user = @config["user"]

  password = @config["password"]

  chat.chat_message("MANUBOT SAYS: Givin' shit!")

  filename = "osp_backup_#{Time.now.strftime('%d%m%H%M%S')}.sql"
  backup_db = `ssh deploy@#{server} 'mysqldump --opt --host=#{host}  --password=#{password} --user=#{user} oup_osp  --default-character-set=latin1 > #{filename}'`

  password = rand(36**8).to_s(36)
  encript_db =`ssh deploy@#{server} 'tar cvzf - #{filename} | openssl des3 -salt -k #{password} | dd of=#{filename}.shit'`

  move_db = `ssh deploy@#{server} 'rm #{filename} && mv #{filename}.shit /data/oup_osp/current/public'`

  # @chat.chat_message("MANUBOT SAYS: Download url => http://#{balance}/#{filename}.shit")
  chat.chat_message("MANUBOT SAYS: Download url => http://#{server}/#{filename}.shit")
  # @chat.chat_message("MANUBOT SAYS: Download url (alternative 2) => http://#{slave}/#{filename}.shit")

  chat.chat_message("MANUBOT SAYS: Warning! This shit is pass protected.")
  chat.chat_message("MANUBOT SAYS: Unprotect it with 'dd if=#{filename}.shit |openssl des3 -d -k #{password} |tar xvzf - '")
end

def working_on_test(message, chat)
  server = @config["server"]
  last_modified_in_server = `ssh deploy@#{server} 'stat -c %Y /data/oup_osp/current/log/staging.log'`

  time = Time.at(Time.now - Time.at(last_modified_in_server.to_i)).gmtime.strftime('%Hh %Mm %Ss')
  message = `ssh deploy@#{server} 'tail -24 /data/oup_osp/current/log/staging.log |grep -c notifications'`

  chat_message = "MANUBOT INFORMATION CENTER: Last update of test's log #{time} ago"
  if !message.empty?
    chat_message += ". #{message} minute/s without human activity"
  else
   chat_message += " and is no a notification"
  end

  chat.chat_message(chat_message)
end

def working_on_test_message(message, chat)
  server = @config["server"]
  message = `ssh deploy@#{server} 'tail -4 /data/oup_osp/current/log/staging.log'`
  chat.chat_message("MANUBOT INFORMATION CENTER: Last update of test's log #{time} ago")

end

def working_on_test_ngnix(message, chat)
  server = @config["server"]

  ngnix_log = `ssh deploy@#{server} 'tail -100 /var/log/nginx/oup_osp.access.log |grep -v system/images'`
  ngnix_lines = ngnix_log.split("\n")

  ngnix_lines.delete_if{|x| x.include?("/api/notifications")}
  ngnix_lines.delete_if{|x| x.match('/api/courses/(.)/digital_books/')}

  automated = 100 - ngnix_lines.size


  #First_time
  first_time, diff_time, first_route = get_ngnix_info(ngnix_lines.first)
  chat.chat_message("Checking a #{diff_time} period. #{automated} automatic calls (of 100)")
  times = ngnix_lines.size < 3 ? ngnix_lines.size : 3
  if times > 0
    chat.chat_message("Lastest #{times} human calls")
    ngnix_lines.last(times).each do |line|
      time, diff, route = get_ngnix_info(line)
      chat.chat_message("#{diff} ago => #{route}")
    end
  end

end

def ci(message, chat)
  Dir.chdir("repos/ouposp")
  $stderr = File.new('jasmine.out', 'w')
  $stderr.sync = true
  jasmine = `guard-jasmine -s jasmine_gem -p 8888 -t 280 -e test -u http://localhost:8888/ > jasmine.out`
end

def test_status(message,chat)
  pull_repo = `cd repos/ouposp && git pull && cd ..`
  terminal_results = `osascript -e '#{'tell application "Terminal" to return name of window 2'}'`
  chat.chat_message("MANUBOT SAYS: Test status #{terminal_results}")
end

def gimme_logshit(message, chat)
  server = @config["server"]
  filename = "staging_#{Time.now.strftime('%d%m%H%M%S')}.log"
  copy_log = `ssh deploy@#{server} 'cp /data/oup_osp/current/log/staging.log /data/oup_osp/current/public/#{filename}'`
  chat.chat_message("MANUBOT SAYS: Download url => http://#{server}/#{filename}")
end




