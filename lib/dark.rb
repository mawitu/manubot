# encoding: UTF-8

TRIGGERS = {} if !defined?(TRIGGERS)
TRIGGERS.merge!({
  "shrug" => {
    :method  => "woot",
    :status  => 'dark',
    :description => '¯\_(ツ)_/¯'
  },
  "manubot" => {
    :method  => 'fortune',
    :status  => 'dark',
    :description => "It's me!"
  },
  "mb: "    => {
    :method  => "cleverbot",
    :status  => 'dark',
    :description => "Ask whatever you want"
  },
  # "cn" => "icndb",
  "rjoke"   => {
    :method  => "joke",
    :status  => 'dark',
    :description => "I always have a joke to tell you"
  },
  "d20"     => {
    :method  => 'd20',
    :status  => 'dark',
    :description => "Are you in troubles?"
  }
})

def woot(message, chat)
  woot = <<-EOF
  ¯\\_(ツ)_/¯
  EOF
  chat.chat_message(woot)
end

def fortune(message, chat)
  if message.body.include?("joke")
    fortune = `fortune /usr/local/Cellar/fortune/9708/share/games/fortunes/humorix-misc`
  else
    fortune = `fortune -a`
  end
  chat.chat_message("MANUBOT WISDOM: #{fortune}")
end

def cleverbot(message, chat)
  message = message.body.gsub("mb: ","")
  response = @cleverbot_client.write(message)
  chat.chat_message("#{response}")
end

def icndb(message, chat)
  osp_team_chat = get_chat_by_id("#cristinamussom/$3139ca5277656e65")
  member = osp_team_chat.activemembers.split(" ").sample
  fullname = get_fullname(member)
  joke = JSON.parse(`curl --silent 'http://api.icndb.com/jokes/random?firstName=#{fullname}&lastName='`)["value"]["joke"]
  osp_team_chat.chat_message("MANUBOT WISDOM: #{joke}")
  #@chat.chat_message("MANUBOT WISDOM: #{joke}")
end

def joke(message, chat)
  type = ['o', 'j', 's', 'p', 'q'].sample
  data = `curl --silent 'http://www.jokes2go.com/cgi-bin/includejoke.cgi?type=#{type}'`
  joke = data.split("');\nthis.document.write")[0].split("this.document.write('")[1]
  chat.chat_message("MANUBOT WISDOM: #{joke}")
end

def d20(message, chat)

replies = {1 => "It works on my machine",
2 => "Where were you when the program blew up?",
3 => "Why do you want to do it that way?",
4 => "You can't use that version on your system",
5 => "Even though it doesn't work, how does it feel?",
6 => "Did you check for a virus on your system?",
7 => "Somebody must have changed my code",
8 => "It works, but it hasn't been tested",
9 => "THIS can't be the source of THAT",
10 => "I can't test everything!",
11 => "It's just some unlucky coincidence",
12 => "You must have the wrong version",
13 => "I haven't touched that module in weeks!",
14 => "There has to be something funky in your data",
15 => "What did you type in wrong to get it to crash?",
16 => "It must be a hardware problem",
17 => "How is that possible?",
18 => "It worked yesterday",
19 => "It's never done that before",
20 => "That's weird"}
reply = replies[(rand * 20 + 1).to_i]    

chat.chat_message("#{reply}")

end
