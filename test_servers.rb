# encoding: UTF-8

require 'rubygems'
require 'mechanize'
require 'pry'

def launch_browser(pages = 10)

  start_time = Time.now

  delay = sleep(rand(10))
  p "Starting test... (delayed #{delay}s)"


  # @server = "http://osp.balanced.avallain.com/"
  @server = "http://localhost:3000/"


  agent = Mechanize.new

  page = agent.get(@server + "app/")
  login = page.form

  login["user[login]"] = "ehoover"
  login["user[password]"] = "123456"
  login["user[institution_code]"] = "se0"
  page = agent.submit(login)

  routes = [
  "api/modules",
  "api/courses",
  "api/markbooks/stats",
  "api/courses/3/modules",
  "api/courses/3/markbooks/stats",
  "api/courses/3/structures",
  "api/courses/3",
  "api/courses/3/contents/search?&page=1&sort=&order=&query=&module=resources&tags=&user=&parent=&type=&source=&used_as=&filter=all+resources&content_ids=",
  "api/courses/3/contents/search?&page=1&sort=&order=&query=&module=planning&tags=&user=&parent=&type=&source=&used_as=&filter=all+plannings&content_ids=",
  "api/courses/3/digital_books",
  "api/courses/3/markbooks?&page=1&sort=&order=&query=&user=&parent=&filter=current",
  "api/courses/3/contents/search?&page=1&sort=&order=&query=&module=Assignments&tags=&user=&parent=&type=&source=&used_as=&filter=all+teacher_assignments&content_ids=",
  "api/courses/3/users/search?&page=1&sort=&order=&query=&filter=all+users&parent",
  "api/courses/3/contents/search?&page=1&sort=&order=&query=&module=Resources%202&tags=&user=&parent=&type=&source=&used_as=&filter=all+resources&content_ids=",
  "api/courses/3/contents/search?&page=1&sort=&order=&query=&module=resources&tags=star&user=true&parent=&type=&source=&used_as=&filter=favourites&content_ids=",
  "api/courses/3/contents/search?&page=1&sort=&order=&query=&module=resources&tags=&user=true&parent=&type=&source=&used_as=&filter=my_resources&content_ids=",
  "api/courses/3/contents/search?&page=1&sort=&order=&query=&module=resources&tags=&user=&parent=70&type=&source=&used_as=&filter=&content_ids=",
  "api/courses/3/contents/search?&page=1&sort=&order=&query=&module=resources&tags=&user=&parent=126&type=&source=&used_as=&filter=&content_ids=",
  "api/courses/3/contents/search?&page=1&sort=&order=&query=&module=Assignments&tags=&user=true&parent=&type=&source=&used_as=&filter=my_assignments&content_ids=",
  "api/courses/3/markbooks?&page=1&sort=&order=&query=&user=&parent=&filter=due_today",
  "api/courses/3/markbooks?&page=1&sort=&order=&query=&user=&parent=&filter=due_this_week",
  "api/courses/3/markbooks?&page=1&sort=&order=&query=&user=&parent=&filter=markbook_past_due_date",
  "api/courses/3/markbooks?&page=1&sort=&order=&query=&user=&parent=&filter=manual_marking",
  "api/courses/3/markbooks?&page=1&sort=&order=&query=&user=&parent=&filter=completed",
  "api/courses/3/markbooks?&page=1&sort=&order=&query=&user=&parent=&filter=all+markbooks",
  "app/courses/3",
  "app/courses/3/modules/resources",
  "app/courses/3/modules/planning",
  "app/courses/3/modules/Digital%20Book",
  "app/courses/3/modules/Assignments",
  "app/courses/3/modules/markbook",
  "app/courses/3/modules/users",
  "app/courses/3/modules/Resources%202"]

  pages.times do |t|
    route = routes.sample
    begin
      sleep(rand(5))
      page = agent.get(@server + route)
      if !page.response["status"] == "200"
        p page.status
        p page.body
      end
    rescue Exception => e
      p route
    end
  end


  #loging out
  page = agent.get(@server + "users/logout")
  end_time = Time.now

  p "Done! Total time: #{end_time - start_time}s"
end


(1..3).map{Thread.new{launch_browser(2)}}.each(&:join)