# encoding: UTF-8

require 'skyper'
require 'yaml'
require 'pry'
require 'sqlite3'

binding.pry

require 'google_drive'



# Load configuration file
@config = YAML::load(File.open('config.yml'))

# Load configuration variables
db = SQLite3::Database.new @config["db_location"]
log = ""

team_chat_id = db.execute("select * from Conversations where displayname='OSP PROJECT TEAM CHAT'").flatten.first

messages = db.execute("select from_dispname,body_xml,timestamp from Messages where convo_id = #{team_chat_id}")

messages.each do |message|
  author = message[0]
  text = message[1]
  time = Time.at(message[2]).strftime("%d-%m-%y %H:%M:%S")
  if text != nil
    log += "#{author} (#{time})\n"
    log += "#{text}\n"
    log += "------------------------------\n"
  end
end

binding.pry

session = GoogleDrive.login("mawitu@gmail.com", "")

file = session.file_by_title("OSP TEAM CHAT LOG")



# Updates content of the remote file.
file.update_from_string(log)


