# encoding: UTF-8

require 'skyper'
require 'rb-fsevent'
require 'cleverbot'
require 'yaml'
require 'pry'
$0 = "Manubot"
require 'json'
Dir.glob(File.dirname(__FILE__) + '/lib/*', &method(:require))

# Load configuration file
@config = YAML::load(File.open('config.yml'))

# Load configuration variables
skype_db_path = @config["skype_db_path"]
@last_id = 0
@last_check = Time.now
@cleverbot_client = Cleverbot::Client.new
@osp_team_chat = get_chat_by_id(@config["osp_team_chat"])
fsevent = FSEvent.new
options = ['--latency', '1.5']


# fsevent main loop. Every time a file inside skype logs directory is changed, is fired
fsevent.watch skype_db_path, options do |directories|

  # Keep last check time to avoid loading unecesary messages and keep a "queue" to process
  messages = get_messages_from(@last_check)
  @last_check = Time.now if messages.size > 0

  puts "#{messages.size} new from #{messages.map(&:from_dispname)}" if messages.size > 0

  messages.each do |message|
    begin
     proc_message(message)
     #10% prosibility of trigger a random chuck norris quote(with 1 message, it scales with the number of messages)
     # icndb(message) if Random.rand(1000*messages.size) < 2
    rescue Exception => e
     puts "Fail #{e}"
    end
  end



end

fsevent.run
